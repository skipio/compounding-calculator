var contribution = document.querySelector('#contrib_value');
var initial = document.querySelector('#initial_value');
var rate = document.querySelector('#rate_value');
var duration = document.querySelector('#duration_value');
var output = document.querySelector('.output');
var flat = document.querySelector('#flat_amount');
var compounded = document.querySelector('#compounded_amount');

const calculateButton = document.querySelector('#calc');

var calcAmount = (initialAmount, monthlyContribution, annualRate, durationYears) => {
  var monthlyRate = (annualRate ** (1 / 12)) / 100;
  var durationMonths = durationYears * 12;

  for (let i = 0; i < durationMonths; i++) {
    initialAmount += monthlyContribution;
    initialAmount *= 1 + monthlyRate;
  }

  return initialAmount;
}

calculateButton.addEventListener('click', () => {
  
  var finalAmount = calcAmount(
    Number(initial.value), 
    Number(contribution.value), 
    Number(rate.value), 
    Number(duration.value)
  );
    
  output.classList.remove('hidden');
  // console.log(compounded.innerHTML);
  compounded.innerHTML = finalAmount.toFixed(2);
  flat.innerHTML = (Number(initial.value) + (Number(duration.value) * 12 * Number(contribution.value))).toFixed(2);
    
});